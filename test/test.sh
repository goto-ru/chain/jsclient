#!/bin/sh

export API_ENDPOINT=http://localhost:8888/api/services/cryptocurrency/v1/wallets/transaction

# FIXME: replace with pipes / streaming

# generate txs
for _ in {1..100};
	do node dist/bundle.test.js >> /tmp/a;
done;

# send 'em
while read tx
do
	echo $tx | curl -H "Content-Type: application/json" -X POST -d @- $API_ENDPOINT
done < /tmp/a
