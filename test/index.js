const Exonum = require('exonum-client');
const lib = require('../lib');

const keys = Exonum.keyPair();

const walletData = {
    pub_key: keys.publicKey,
    name: "Someone",
    age: "25",
    email: "example@example.com",
    city: "DC",
    school: "1337",
};

console.log(lib.TxCreateWallet.json(walletData, keys.secretKey));

console.log(lib.TxAssignTask.json({
        task_hash: keys.publicKey, // invalid hash
        author: keys.publicKey,
    }, keys.secretKey));
