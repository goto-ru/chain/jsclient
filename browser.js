const Exonum = require('exonum-client');
const lib = require('./lib');

const keys = Exonum.keyPair();

const walletData = {
    pub_key: keys.publicKey,
    name: "Someone"
};

const tx = lib.TxCreateWallet.json(walletData, keys.secretKey);

fetch('http://127.0.0.1:8888/api/services/cryptocurrency/v1/wallets/transaction', {method: 'POST', body: tx})
    .then(result => {
        console.log(result);
    })
    .catch(error => {
         console.log(error);
});
