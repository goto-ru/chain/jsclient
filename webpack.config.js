const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = [
  {
    entry: './node.js',
    target: 'node',
    externals: [nodeExternals()],
    output: {
      filename: 'bundle.node.js',
      library: '@goto-ru/chain-jsclient',
      libraryTarget: 'umd',
      path: path.resolve(__dirname, 'dist')
    }
  },
  {
    entry: './test',
    target: 'node',
    output: {
      filename: 'bundle.test.js',
      path: path.resolve(__dirname, 'dist')
    }
  },
  {
    entry: './browser.js',
    target: 'web',
    output: {
      filename: 'bundle.browser.js',
      library: '@goto-ru/chain-jsclient',
      libraryTarget: 'umd',
      path: path.resolve(__dirname, 'dist')
    }
  },
];
