const Exonum = require('exonum-client');

export const TxCreateWallet = Exonum.newMessage({
    network_id: 0,
    protocol_version: 0,
    service_id: 1,
    message_id: 1,
    size: 72,
    fields: {
        pub_key: {type: Exonum.PublicKey, size: 32, from: 0, to: 32},
        name: {type: Exonum.String, size: 8, from: 32, to: 40},
        age: {type: Exonum.String, size: 8, from: 40, to: 48},
        email: {type: Exonum.String, size: 8, from: 48, to: 56},
        city: {type: Exonum.String, size: 8, from: 56, to: 64},
        school: {type: Exonum.String, size: 8, from: 64, to: 72},
    }
});

export const TxCreateTask = Exonum.newMessage({
    network_id: 0,
    protocol_version: 0,
    service_id: 1,
    message_id: 69,
    size: 24,
    fields: {
        name: {type: Exonum.String, size: 8, from: 0, to: 8},
        desc: {type: Exonum.String, size: 8, from: 8, to: 16},
        reward: {type: Exonum.Uint64, size: 8, from: 16, to: 24},
    }
});

export const TxCloseTask = Exonum.newMessage({
    network_id: 0,
    protocol_version: 0,
    service_id: 1,
    message_id: 70,
    size: 32,
    fields: {
        task_hash: {type: Exonum.Hash, size: 32, from: 0, to: 32},
    }
});

export const TxAssignTask = Exonum.newMessage({
    network_id: 0,
    protocol_version: 0,
    service_id: 1,
    message_id: 74,
    size: 64,
    fields: {
        task_hash: {type: Exonum.Hash, size: 32, from: 0, to: 32},
        author: {type: Exonum.PublicKey, size: 32, from: 32, to: 64},
    }
});

export const TxSubmitSolution = Exonum.newMessage({
    network_id: 0,
    protocol_version: 0,
    service_id: 1,
    message_id: 71,
    size: 40,
    fields: {
        solution_hash: {type: Exonum.Hash, size: 8, from: 0, to: 32},
        url: {type: Exonum.String, size: 8, from: 32, to: 40},
    }
});
