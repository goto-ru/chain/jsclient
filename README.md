# goto-ru/chain/jsclient

JS Exonum type definitions / helper functions for interfacing with blockchain API.

## Usage

Enter dev environment:

```sh
nix-shell
```

### Run test

```sh
sh test/test.sh
```
