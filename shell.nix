with import <nixpkgs> {};

stdenv.mkDerivation {
	name = "goto-ru.chain.jsclient";
	buildInputs = [
		nodejs
		yarn
	];
	shellHook = ''
		export PATH="$PWD/node_modules/.bin/:$PATH"
		yarn
		webpack
	'';
}
